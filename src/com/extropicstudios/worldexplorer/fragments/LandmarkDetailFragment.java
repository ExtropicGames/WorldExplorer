package com.extropicstudios.worldexplorer.fragments;

import com.extropicstudios.worldexplorer.R;
import com.extropicstudios.worldexplorer.models.Landmark;
import com.extropicstudios.worldexplorer.models.LandmarkDB;
import com.google.android.maps.GeoPoint;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LandmarkDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    Landmark mItem;

    public LandmarkDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = LandmarkDB.landmarkMap.get(getArguments().getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_landmark_detail, container, false);
        if (mItem != null) {
            final TextView text = (TextView) rootView.findViewById(R.id.landmark_detail);
            final GeoPoint loc = mItem.getLocation();
            text.setText(mItem.getName() + "\nLatitude: " + loc.getLatitudeE6() + "\nLongitude: " + loc.getLongitudeE6());
        }
        return rootView;
    }
}
