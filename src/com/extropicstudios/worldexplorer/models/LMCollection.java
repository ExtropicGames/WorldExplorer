package com.extropicstudios.worldexplorer.models;

import java.util.ArrayList;
import java.util.List;

public class LMCollection {
    public String name;
    public List<Landmark> landmarks = new ArrayList<Landmark>();
    public List<LMCollection> subcollections = new ArrayList<LMCollection>();

    @Override
    public String toString() {
        return name;
    }
}
