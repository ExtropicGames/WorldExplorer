package com.extropicstudios.worldexplorer.models;

import com.google.android.maps.GeoPoint;

public class Landmark {
    private final GeoPoint location;
    private final String name;

    public Landmark(int latitude, int longitude, String name) {
        location = new GeoPoint(latitude, longitude);
        this.name = name;
    }

    public GeoPoint getLocation() { return location; }
    public String getName() { return name; }

    @Override
    public String toString() { return name + " <" + location + ">"; }
}
