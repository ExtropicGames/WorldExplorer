package com.extropicstudios.worldexplorer.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LandmarkDB {
    public static List<LMCollection> collectionList = new ArrayList<LMCollection>();
    public static Map<String, LMCollection> collectionMap = new HashMap<String, LMCollection>();
    public static Map<String, Landmark> landmarkMap = new HashMap<String, Landmark>();

    // for testing purposes only
    static {
        Landmark l = new Landmark(0,0,"Test");
        landmarkMap.put(l.getName(), l);
        LMCollection c = new LMCollection();
        c.name = "North America";
        c.landmarks.add(l);
        collectionList.add(c);
        collectionMap.put(c.name, c);
    }
}
