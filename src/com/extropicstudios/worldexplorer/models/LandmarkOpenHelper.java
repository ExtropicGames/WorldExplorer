package com.extropicstudios.worldexplorer.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LandmarkOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "LandmarkDB";

    public LandmarkOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE landmarks (name VARCHAR(255), lat INT, lon INT)");
        db.execSQL("INSERT INTO landmarks VALUES ('North America', 0, 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // don't need to do anything yet because there are no old versions to upgrade from
    }
}
