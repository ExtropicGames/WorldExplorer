package com.extropicstudios.worldexplorer.activities;

import com.extropicstudios.worldexplorer.R;
import com.extropicstudios.worldexplorer.fragments.LandmarkDetailFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class LandmarkDetailActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landmark_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString(LandmarkDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(LandmarkDetailFragment.ARG_ITEM_ID));
            LandmarkDetailFragment fragment = new LandmarkDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.landmark_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, ExplorationActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
