package com.extropicstudios.worldexplorer.activities;

import com.extropicstudios.worldexplorer.R;
import com.extropicstudios.worldexplorer.Strings;
import com.extropicstudios.worldexplorer.fragments.LandmarkDetailFragment;
import com.extropicstudios.worldexplorer.fragments.LandmarkListFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ExplorationActivity extends FragmentActivity implements LandmarkListFragment.Callbacks {

    private boolean mTwoPane;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landmark_list);

        final LandmarkListFragment listFragment = (LandmarkListFragment) getSupportFragmentManager().findFragmentById(R.id.landmark_list);
        
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String collectionID = extras.getString(Strings.EXTRA_COLLECTION_ID);
            listFragment.setCollection(collectionID);
        }
        
        if (findViewById(R.id.landmark_detail_container) != null) {
            mTwoPane = true;
            listFragment.setActivateOnItemClick(true);
        }
    }

    @Override
    public void onItemSelected(String id) {
        // TODO: determine whether item selected was a landmark or a collection, and
        // go to the appropriate activity
        if (mTwoPane) {
            Bundle arguments = new Bundle();
            arguments.putString(LandmarkDetailFragment.ARG_ITEM_ID, id);
            LandmarkDetailFragment fragment = new LandmarkDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.landmark_detail_container, fragment).commit();
        } else {
            Intent detailIntent = new Intent(this, LandmarkDetailActivity.class);
            detailIntent.putExtra(LandmarkDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_explore, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionbar_mapview) {
            Intent intent = new Intent(this, MapViewActivity.class);
            startActivity(intent);
            return true;
        }

        return false;
    }
}
